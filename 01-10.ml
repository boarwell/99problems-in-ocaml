let rec last = function [] -> None | [x] -> Some x | _ :: t -> last t

let rec last_two = function
  | [] | [_] -> None
  | [a; b] -> Some (a, b)
  | h :: t -> last_two t


(*自分で考えた*)
let rec at n l =
  match (n, l) with
  | 1, h :: _ -> Some h
  | x, _ :: l -> at (n - 1) l
  | _, [] -> None


let rec at' k = function
  | [] -> None
  | h :: l -> if k = 1 then h else at' (k - 1) l


let length l =
  let rec f c = function [] -> c | _ :: t -> f (c + 1) t in
  f 0 l


let rev l =
  let rec aux orig rev =
    match orig with [] -> rev | h :: t -> aux t (h :: rev)
  in
  aux l []


(*
もともとのリストとrevしたリストが同一ならtrueでどうでしょうか
*)
let is_palindrome l = l = rev l

type 'a node = One of 'a | Many of 'a node list

let flatten l =
  let rec aux acc = function
    | [] -> acc
    | (One x) :: t -> aux (x :: acc) t
    | (Many l) :: t -> aux (aux acc l) t
  in
  List.rev (aux [] l)


(*
自分で考えたけどだめだった
*)
let compress l =
  let rec aux acc = function
    | [] -> acc
    | [x] -> x :: acc
    | a :: b :: t when a = b -> aux (a :: acc) (b :: t)
    | h :: t -> aux (h :: acc) t
  in
  rev (aux [] l)


let rec compress' = function
  (*このパターンは(b::_)をtと名付けている*)
  | a :: (b :: _ as t) ->
      if a = b then compress' t else a :: compress' t
  | smaller -> smaller


(*a :: b :: tとしてしまうとccと2つ続いたものは消えてしまう*)
let rec compress'' = function
  | a :: b :: t -> if a = b then compress'' t else a :: compress'' (b :: t)
  | smaller -> smaller


let pack l =
  let rec aux current acc = function
    | [] -> []
    | [x] -> (x :: current) :: acc
    | a :: (b :: _ as t) ->
        if a = b then aux (a :: current) acc t
        else aux [] ((a :: current) :: acc) t
  in
  List.rev (aux [] [] l)


let pack' l =
  let rec aux current acc = function
    | [] -> []
    | [x] -> (x :: current) :: acc
    | a :: (b :: _ as t) ->
        if a = b then aux (a :: current) acc t
        else aux [] ((a :: current) :: acc) t
  in
  List.rev (aux [] [] l)


(*自分で考えて挫折した
  let encode l = 
  let rec aux current acc = function
    | [] -> []
    | [x] ->
      if snd current = x then (fst current + 1, snd current) :: acc
      else (1, x) :: current :: acc
    | a :: b :: t when a = b -> aux 
*)
let encode list =
  let rec aux count acc = function
    | [] -> []
    | [x] -> (count + 1, x) :: acc
    | a :: (b :: _ as t) ->
        if a = b then aux (count + 1) acc t
        else aux 0 ((count + 1, a) :: acc) t
  in
  List.rev (aux 0 [] list)

